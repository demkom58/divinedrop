package com.demkom58.divinedrop;

import com.demkom58.divinedrop.lang.LangManager;
import com.demkom58.divinedrop.versions.listeners.NewListener;
import com.demkom58.divinedrop.versions.listeners.OldListener;
import com.demkom58.divinedrop.versions.nms.NMSUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class DivineDrop extends JavaPlugin {
    private static DivineDrop instance;

    @Override
    public void onEnable() {
        instance = this;
        Data.lang_manager = new LangManager();

        saveDefaultConfig();
        loadConfig();
        NMSUtil.setupNMS();

        String version = Bukkit.getServer().getVersion();
        if(version.contains("1.8") || version.contains("1.9") ||  version.contains("1.10") || version.contains("1.11")) getServer().getPluginManager().registerEvents(new OldListener(), this);
        else getServer().getPluginManager().registerEvents(new NewListener(), getInstance());

        getCommand("divinedrop").setExecutor(new DivineCommands());

        registerCountdown();

        if(Data.add_items_on_chunk_load)
            for(World world : getServer().getWorlds()) for(Entity entity : world.getEntities())
                if(entity instanceof Item) Data.ITEMS_LIST.add((Item)entity);
    }

    @Override
    public void onDisable() {
        Bukkit.getScheduler().cancelAllTasks();
        Logic.removeTimers();
    }

    public void loadConfig() {
        Data.custom_countdowns = null;
        saveDefaultConfig();
        reloadConfig();

        FileConfiguration conf = getConfig();

        Data.timer_value = getConfig().getInt("timer");
        Data.lang = conf.getString("lang");
        if(Data.lang == null) Data.lang = "en_CA";
        Data.lang_manager.downloadLang(Data.lang);
        Data.format = ChatColor.translateAlternateColorCodes('&', conf.getString("format"));
        Data.lite_format = ChatColor.translateAlternateColorCodes('&', conf.getString("without-countdown-format"));
        Data.no_perm_message = ChatColor.translateAlternateColorCodes('&', conf.getString("no-perms"));
        Data.unknown_cmd_message = ChatColor.translateAlternateColorCodes('&', conf.getString("unknown-cmd"));
        Data.reloaded_message = ChatColor.translateAlternateColorCodes('&', conf.getString("reloaded"));
        Data.item_display_name_message = ChatColor.translateAlternateColorCodes('&', conf.getString("dname"));
        Data.enable_custom_countdowns = conf.getBoolean("enable-custom-countdowns");
        Data.add_items_on_chunk_load = conf.getBoolean("timer-for-loaded-items");
        Data.pickup_on_shift = conf.getBoolean("pickup-items-on-sneak");
        Data.save_player_death_dropped_items = conf.getBoolean("save-player-dropped-items");

        if(Data.save_player_death_dropped_items) Data.death_dropped_items_list = new ArrayList<>();
        if(Data.enable_custom_countdowns) {
            Data.custom_countdowns = new HashMap<>();
            ConfigurationSection sec = conf.getConfigurationSection("custom-countdowns");
            for(String mater : sec.getKeys(false)) {
                String name = sec.getString(mater+".name-filter");
                if(name == null) name = "*";
                name = ChatColor.translateAlternateColorCodes('&', name);
                Integer timer = sec.getInt(mater+".timer");
                String format = sec.getString(mater+".format");
                if(format == null) format = Data.format;
                format = ChatColor.translateAlternateColorCodes('&', format);
                Material material = Material.getMaterial(mater);
                HashMap<String, DataContainer> itemFilter;
                if(!Data.custom_countdowns.containsKey(material)) {
                    itemFilter = new HashMap<>();
                    Data.custom_countdowns.put(material, itemFilter);
                } else { itemFilter = Data.custom_countdowns.get(material); }
                itemFilter.put(name, new DataContainer(timer, format));
            }
        }
    }

    private void registerCountdown() {
        getServer().getScheduler().runTaskTimer(this, () -> {
            for (Item item : Data.ITEMS_LIST) {
                getServer().getScheduler().runTaskAsynchronously(getInstance(), () -> {
                    List<MetadataValue> metadataCountdowns = item.getMetadata(Data.METADATA_COUNTDOWN);
                    if (metadataCountdowns.isEmpty()) {
                        int timer = Data.timer_value;
                        String format = Data.format;
                        String name = item.getItemStack().getItemMeta().getDisplayName();
                        if(name == null) name = "";
                        Material material = item.getItemStack().getType();
                        if (Data.enable_custom_countdowns) {
                            Boolean mapContainsMaterial = Data.custom_countdowns.containsKey(material);
                            if (mapContainsMaterial) {
                                HashMap<String, DataContainer> filterMap = Data.custom_countdowns.get(material);
                                Boolean mapContainsName = filterMap.containsKey(name);
                                if (mapContainsName) {
                                    timer = filterMap.get(name).getTimer();
                                    format = filterMap.get(name).getFormat();
                                }
                                Boolean mapContainsVoid = filterMap.containsKey("");
                                if (mapContainsVoid & name.equals("") & !mapContainsName) {
                                    timer = filterMap.get("").getTimer();
                                    format = filterMap.get("").getFormat();
                                }
                                Boolean mapContainsAny = filterMap.containsKey("*");
                                if (mapContainsAny & (!name.equals("") || !mapContainsVoid) & !mapContainsName) {
                                    timer = filterMap.get("*").getTimer();
                                    format = filterMap.get("*").getFormat();
                                }
                            }
                        }
                        DataContainer dataContainer = new DataContainer(timer, format);

                        if(Data.save_player_death_dropped_items) if(Data.death_dropped_items_list.contains(item.getItemStack())) {
                            Logic.setItemWithoutTimer(item, dataContainer);
                            return;
                        }
                        if (timer == -1) {
                            Logic.setItemWithoutTimer(item, dataContainer);
                            return;
                        }
                        Logic.setItemWithTimer(item, dataContainer);
                        return;
                    }
                    DataContainer dataContainer = (DataContainer) metadataCountdowns.get(0).value();
                    dataContainer.setTimer(dataContainer.getTimer() - 1);
                    Logic.setItemWithTimer(item, dataContainer);
                });
            }
        }, 0L, 20L);
    }

    public static DivineDrop getInstance() {
        return instance;
    }
}
