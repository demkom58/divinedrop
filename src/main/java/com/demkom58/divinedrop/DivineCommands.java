package com.demkom58.divinedrop;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DivineCommands implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            if (sender.hasPermission("divinedrop.info")) {
                sender.sendMessage(Data.INFO);
                return true;
            } else {
                sender.sendMessage(Data.PREFIX + Data.no_perm_message);
                return false;
            }
        } else {
            if (args[0].equalsIgnoreCase("reload")) {
                if (!sender.hasPermission("divinedrop.reload")) {
                    sender.sendMessage(Data.PREFIX + Data.no_perm_message);
                    return false;
                }
                DivineDrop.getInstance().loadConfig();
                sender.sendMessage(Data.PREFIX + Data.reloaded_message);
            } else if (args[0].equalsIgnoreCase("getName")) {
                if (sender.hasPermission("divinedrop.getname")) {
                    Player player = (Player) sender;
                    String itemName;
                    try {
                        if (DivineDrop.getInstance().getServer().getVersion().contains("1.8"))
                            itemName = player.getItemInHand().getItemMeta().getDisplayName();
                        else itemName = player.getInventory().getItemInMainHand().getItemMeta().getDisplayName();
                    } catch (NullPointerException ex) {
                        player.sendMessage(Data.PREFIX + Data.item_display_name_message.replace("$name$", "AIR"));
                        return false;
                    }
                    if (itemName == null) itemName = "NONAME";
                    player.sendMessage(Data.PREFIX + Data.item_display_name_message.replace("$name$", itemName.replace('§', '&')));
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("size")) {
                if (sender.hasPermission("divinedrop.developer")) {
                    sender.sendMessage(Data.PREFIX + "Items to remove: " + Data.ITEMS_LIST.size());
                }
            } else sender.sendMessage(Data.PREFIX + Data.unknown_cmd_message);
        }
        return false;
    }
}
