package com.demkom58.divinedrop;

import com.demkom58.divinedrop.lang.LangManager;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class Data {
    private Data(){ }

    public static final String PREFIX = "§5§lDivineDrop §7> §f";

    public static String lang;
    public static String format;
    public static String lite_format;
    public static String no_perm_message;
    public static String unknown_cmd_message;
    public static String reloaded_message;
    public static String item_display_name_message;

    public static boolean enable_custom_countdowns;
    public static boolean add_items_on_chunk_load;
    public static boolean pickup_on_shift;
    public static boolean save_player_death_dropped_items;

    public static int timer_value = 10;

    public static final String TIMER_PLACEHOLDER = "$countDown$";
    public static final String SIZE_PLACEHOLDER = "$size$";
    public static final String NAME_PLACEHOLDER = "$name$";

    public static final String METADATA_COUNTDOWN = "╚countDown";

    public static final String[] INFO = new String[]{
            "§b",
            "§b <----------------------------------> ",
            "§b    §cPLUGIN DEVELOPED BY DEMKOM58     ",
            "§b      §6spigotmc.org/members/98068     ",
            "§b                                       ",
            "§b      /dd reload - reloads config      ",
            "§b   /dd getname - get item custom name  ",
            "§b                                       ",
            "§b             §cCODED WITH ♥            ",
            "§b <----------------------------------> ",
            "§b"
    };

    public static HashMap<Material, HashMap<String, DataContainer>> custom_countdowns;
    public static final List<Item> ITEMS_LIST = new ArrayList<>();
    public static List<ItemStack> death_dropped_items_list;

    public static LangManager lang_manager;

    public static String getLangPath() {
        return DivineDrop.getInstance().getDataFolder().getAbsolutePath() + "/languages/"+Data.lang +".lang";
    }
}
