package com.demkom58.divinedrop.versions.nms;

import com.demkom58.divinedrop.DivineDrop;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class NMSUtil {
    private static NMS nms;

    public static void setupNMS() {
        String ver = null;
        try {
            ver = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
        }   catch (ArrayIndexOutOfBoundsException ex) {
            Bukkit.getConsoleSender().sendMessage("[" + DivineDrop.getInstance().getDescription().getName() + "] " + ChatColor.RED + "NMS cant be loaded! Disabling plugin...");
            Bukkit.getScheduler().cancelAllTasks();
            Bukkit.getPluginManager().disablePlugin(DivineDrop.getInstance());
        }

        if(ver.equals("v1_8_R3")) {
            nms = new NMS_v1_8_R3();
        } else if(ver.equals("v1_9_R1")) {
            nms = new NMS_v1_9_R1();
        } else if(ver.equals("v1_9_R2")) {
            nms = new NMS_v1_9_R2();
        } else if(ver.equals("v1_10_R1")) {
            nms = new NMS_v1_10_R1();
        } else if(ver.equals("v1_11_R1")) {
            nms = new NMS_v1_11_R1();
        } else if(ver.equals("v1_12_R1")) {
            nms = new NMS_v1_12_R1();
        } else {
            Bukkit.getConsoleSender().sendMessage("[" + DivineDrop.getInstance().getDescription().getName() +"] " + ChatColor.RED + " Your current version, " + ver + ", is not supported!");
            Bukkit.getScheduler().cancelAllTasks();
            Bukkit.getPluginManager().disablePlugin(DivineDrop.getInstance());
        }
    }

    public static NMS getNMS() {
        return nms;
    }
}
