package com.demkom58.divinedrop.versions.nms;

import org.bukkit.inventory.ItemStack;

public interface NMS {
    String getI18NDisplayName(ItemStack item);
}
