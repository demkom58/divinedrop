package com.demkom58.divinedrop.versions.nms;

import com.demkom58.divinedrop.lang.LocaleI18n;
import org.bukkit.inventory.ItemStack;

public class NMS_v1_12_R1 implements NMS {

    public String getI18NDisplayName(ItemStack item) {
        if(item == null) return null;
        return getName(item);
    }


    private String getName(ItemStack bItemStack) {
        net.minecraft.server.v1_12_R1.ItemStack itemStack = org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack.asNMSCopy(bItemStack);
        net.minecraft.server.v1_12_R1.NBTTagCompound nbtTagCompound = itemStack.d("display");
        if (nbtTagCompound != null) {
            if (nbtTagCompound.hasKeyOfType("Name", 8)) {
                return nbtTagCompound.getString("Name");
            }
            if (nbtTagCompound.hasKeyOfType("LocName", 8)) {
                return LocaleI18n.get(nbtTagCompound.getString("LocName"));
            }
        }
        return getLangNameNMS(itemStack);
    }

    private String getLangNameNMS(net.minecraft.server.v1_12_R1.ItemStack itemStack) {
        return LocaleI18n.get(getNameNMS(itemStack) + ".name").trim();
    }

    private String getNameNMS(net.minecraft.server.v1_12_R1.ItemStack itemStack) {
        return LocaleI18n.get(itemStack.getItem().a(itemStack));
    }
}
