package com.demkom58.divinedrop.versions.nms;

import com.demkom58.divinedrop.lang.LocaleI18n;
import org.bukkit.inventory.ItemStack;

public class NMS_v1_8_R3 implements NMS {
    public String getI18NDisplayName(ItemStack item) {
        if(item == null) return null;
        return getName(item);
    }

    private String getName(ItemStack bItemStack) {
        net.minecraft.server.v1_8_R3.ItemStack itemStack = org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack.asNMSCopy(bItemStack);
        String s = getLangNameNMS(itemStack);
        if (itemStack.getTag() != null && itemStack.getTag().hasKeyOfType("display", 10)) {
            net.minecraft.server.v1_8_R3.NBTTagCompound nbttagcompound = itemStack.getTag().getCompound("display");
            if (nbttagcompound.hasKeyOfType("Name", 8)) {
                s = nbttagcompound.getString("Name");
            }
        }
        return s;
    }

    private String getLangNameNMS(net.minecraft.server.v1_8_R3.ItemStack itemStack) {
        return LocaleI18n.get(getNameNMS(itemStack) + ".name").trim();
    }

    private String getNameNMS(net.minecraft.server.v1_8_R3.ItemStack itemStack) {
        String name = itemStack.getItem().e_(itemStack);
        return name == null ? "" : net.minecraft.server.v1_8_R3.LocaleI18n.get(name);
    }
}
