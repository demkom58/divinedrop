package com.demkom58.divinedrop.versions.nms;

import com.demkom58.divinedrop.lang.LocaleI18n;
import org.bukkit.inventory.ItemStack;

public class NMS_v1_9_R2 implements NMS {
    public String getI18NDisplayName(ItemStack item) {
        if(item == null) return null;
        return getName(item);
    }

    private String getName(ItemStack bItemStack) {
        net.minecraft.server.v1_9_R2.ItemStack itemStack = org.bukkit.craftbukkit.v1_9_R2.inventory.CraftItemStack.asNMSCopy(bItemStack);
        String s = getLangNameNMS(itemStack);
        if (itemStack.getTag() != null && itemStack.getTag().hasKeyOfType("display", 10)) {
            net.minecraft.server.v1_9_R2.NBTTagCompound nbttagcompound = itemStack.getTag().getCompound("display");
            if (nbttagcompound.hasKeyOfType("Name", 8)) {
                s = nbttagcompound.getString("Name");
            }
        }
        return s;
    }

    private String getLangNameNMS(net.minecraft.server.v1_9_R2.ItemStack itemStack) {
        return LocaleI18n.get(getNameNMS(itemStack) + ".name").trim();
    }

    private String getNameNMS(net.minecraft.server.v1_9_R2.ItemStack itemStack) {
        String name = itemStack.getItem().f_(itemStack);
        return name == null ? "" : LocaleI18n.get(name);
    }
}
