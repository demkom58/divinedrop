package com.demkom58.divinedrop.versions.listeners;

import com.demkom58.divinedrop.Data;
import com.demkom58.divinedrop.Logic;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.world.ChunkLoadEvent;

public final class OldListener implements Listener {

    public OldListener() {}

    @EventHandler
    public void onChunkLoad(ChunkLoadEvent event) {
        if(!Data.add_items_on_chunk_load) return;
        Logic.registerNewItems(event.getChunk().getEntities());
    }

    @EventHandler
    public void onDropDeSpawn(ItemDespawnEvent event) {
        if(Data.save_player_death_dropped_items) Data.death_dropped_items_list.remove(event.getEntity().getItemStack());
        Data.ITEMS_LIST.remove(event.getEntity());
    }

    @EventHandler
    public void onDropPickup(PlayerPickupItemEvent event) {
        if(Data.pickup_on_shift) if(!event.getPlayer().isSneaking()) {
            event.setCancelled(true);
            return;
        }
        if(Data.save_player_death_dropped_items) Data.death_dropped_items_list.remove(event.getItem().getItemStack());
        Data.ITEMS_LIST.remove(event.getItem());
    }

    @EventHandler
    public void onDeathDrop(PlayerDeathEvent event) {
        if(Data.save_player_death_dropped_items) {
            Logic.registerDeathDrop(event);
        }
    }

    @EventHandler
    public void onSpawnDrop(ItemSpawnEvent event) {
        Data.ITEMS_LIST.add(event.getEntity());
        event.getEntity().setCustomNameVisible(true);

    }

    @EventHandler
    public void onMergeDrop(ItemMergeEvent event) {
        Data.ITEMS_LIST.remove(event.getEntity());
    }
}