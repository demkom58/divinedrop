package com.demkom58.divinedrop.versions.listeners;

import com.demkom58.divinedrop.Data;
import com.demkom58.divinedrop.Logic;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.world.ChunkLoadEvent;

public final class NewListener implements Listener {

    public NewListener() {}

    @EventHandler
    public void onChunkLoad(ChunkLoadEvent event) {
        if(!Data.add_items_on_chunk_load) return;
        Logic.registerNewItems(event.getChunk().getEntities());
    }

    @EventHandler
    public void onDropDeSpawn(ItemDespawnEvent event) {
        if(Data.save_player_death_dropped_items) Data.death_dropped_items_list.remove(event.getEntity().getItemStack());
        Data.ITEMS_LIST.remove(event.getEntity());
    }

    @EventHandler
    public void onDropPickup(EntityPickupItemEvent event) {
        if(Data.pickup_on_shift) if(event.getEntity() instanceof Player) if(!(((Player)event.getEntity()).isSneaking())) {
            event.setCancelled(true);
            return;
        }
        if(Data.save_player_death_dropped_items) Data.death_dropped_items_list.remove(event.getItem().getItemStack());
        Data.ITEMS_LIST.remove(event.getItem());
    }


    @EventHandler
    public void onDeathDrop(PlayerDeathEvent event) {
        if(Data.save_player_death_dropped_items) {
            Logic.registerDeathDrop(event);
        }
    }


    @EventHandler
    public void onSpawnDrop(ItemSpawnEvent event) {
        Data.ITEMS_LIST.add(event.getEntity());
        event.getEntity().setCustomNameVisible(true);

    }

    @EventHandler
    public void onMergeDrop(ItemMergeEvent event) {
        Data.ITEMS_LIST.remove(event.getEntity());
    }
}