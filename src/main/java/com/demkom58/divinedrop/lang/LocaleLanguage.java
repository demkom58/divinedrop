package com.demkom58.divinedrop.lang;

import com.demkom58.divinedrop.Data;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.IllegalFormatException;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.commons.io.IOUtils;

public class LocaleLanguage {
    private static LocaleLanguage instance;
    private static Pattern PATTERN = Pattern.compile("%(\\d+\\$)?[\\d\\.]*[df]");
    private static Splitter SPLITTER = Splitter.on('=').limit(2);
    private final Map<String, String> langMap = Maps.newHashMap();
    private long curTimeMillis;

    public LocaleLanguage() {
        try {
            InputStream inputStream = new FileInputStream(Data.getLangPath());
            Iterator iterator = IOUtils.readLines(inputStream, StandardCharsets.UTF_8).iterator();
            while (iterator.hasNext()) {
                String next = (String) iterator.next();
                if (!next.isEmpty() && next.charAt(0) != '#') {
                    String[] array = Iterables.toArray(SPLITTER.split(next), String.class);
                    if (array != null && array.length == 2) {
                        String first = array[0];
                        String str = PATTERN.matcher(array[1]).replaceAll("%$1s");
                        this.langMap.put(first, str);
                    }
                }
            }
            this.curTimeMillis = System.currentTimeMillis();
        } catch (IOException ignored) { }
        instance = this;
        new LocaleI18n();
    }

    public static LocaleLanguage getInstance() {
        return instance;
    }

    public synchronized String getName(String path) {
        return this.getLangName(path);
    }

    public synchronized String getName(String path, Object... objects) {
        String name = this.getLangName(path);
        try {
            return String.format(name, objects);
        } catch (IllegalFormatException ex) {
            return "Format error: " + name;
        }
    }

    private String getLangName(String path) {
        String result = this.langMap.get(path);
        return result == null ? path : result;
    }

    public synchronized boolean contains(String path) {
        return this.langMap.containsKey(path);
    }

    public long getCurrentMillis() {
        return this.curTimeMillis;
    }
}
