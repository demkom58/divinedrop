package com.demkom58.divinedrop.lang;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import com.demkom58.divinedrop.Data;
import com.demkom58.divinedrop.DivineDrop;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.stream.JsonReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;

public class Downloader {

    private final static String VERSIONS_LIST = "https://launchermeta.mojang.com/mc/game/version_manifest.json";
    private final static String ASSETS_URL = "http://resources.download.minecraft.net/";
    private final Gson gson = new Gson();
    private final DivineDrop plugin;

    public Downloader(DivineDrop plugin) {
        this.plugin = plugin;
    }

    public void downloadResource(String locale, File destination) throws IOException {
        VersionManifest vm = this.downloadObject(new URL(Downloader.VERSIONS_LIST), VersionManifest.class);
        ClientVersion client = this.downloadObject(new URL(vm.getLatestRelease().getUrl()), ClientVersion.class);
        AssetIndex ai = this.downloadObject(new URL(client.getAssetUrl()), AssetIndex.class);
        String hash = ai.getLocaleHash(locale);
        this.plugin.getServer().getConsoleSender().sendMessage("§eDownloading §6{0}.lang §efile (hash: §6{1}§e)".replace("{0}", locale).replace("{1}", hash));
        FileUtils.copyURLToFile(new URL(Downloader.ASSETS_URL + this.createPathFromHash(hash)), destination);
    }

    private <T extends Object> T downloadObject(URL url, Class<T> object) throws IOException {
        try (InputStream inputStream = url.openConnection().getInputStream();
             InputStreamReader r = new InputStreamReader(inputStream);
             JsonReader jr = new JsonReader(r)) {
            return this.gson.fromJson(jr, object);
        }
    }

    private String createPathFromHash(String hash) {
        return hash.substring(0, 2) + "/" + hash;
    }

    class VersionManifest {
        @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
        private LinkedTreeMap<String, String> latest;
        private ArrayList<RemoteClient> versions;

        public RemoteClient getLatestRelease() {
            String release = this.latest.get("release");
            for (RemoteClient c : this.versions) {
                if (c.getId().equals(release)) {
                    return c;
                }
            }

            throw new IllegalArgumentException(release + " does not exists. There something is definitely wrong.");
        }
    }

    class RemoteClient {
        private String id, url;

        public String getId() {
            return id;
        }

        public String getUrl() {
            return url;
        }
    }

    class ClientVersion {
        @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
        private LinkedTreeMap<String, String> assetIndex;

        public String getAssetUrl() {
            return this.assetIndex.get("url");
        }
    }

    class AssetIndex {
        private final static String PATH = "minecraft/lang/%s.lang";
        @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
        private LinkedTreeMap<String, LinkedTreeMap<String, String>> objects;

        public String getLocaleHash(String locale) {
            LinkedTreeMap<String, String> asset = this.objects.get(String.format(PATH, locale.toLowerCase()));
            if (asset == null) {
                Data.lang = "en_CA";
                Data.lang_manager.downloadLang(Data.lang);
            }
            return asset.get("hash");
        }
    }
}