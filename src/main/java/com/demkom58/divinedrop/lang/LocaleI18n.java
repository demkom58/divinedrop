package com.demkom58.divinedrop.lang;

public class LocaleI18n {

    private static LocaleLanguage localeLanguage;

    public LocaleI18n() {
        localeLanguage = LocaleLanguage.getInstance();
    }

    public static String get(String path) {
        return localeLanguage.getName(path);
    }
    public static String getLocName(String path, Object... objects) {
        return localeLanguage.getName(path, objects);
    }
    public static String getLocName(String path) {
        return localeLanguage.getName(path);
    }
    public static boolean contains(String path) {
        return localeLanguage.contains(path);
    }
    public static long getMillis() {
        return localeLanguage.getCurrentMillis();
    }
}
