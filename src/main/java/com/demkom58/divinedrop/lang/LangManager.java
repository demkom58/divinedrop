package com.demkom58.divinedrop.lang;

import com.demkom58.divinedrop.Data;
import com.demkom58.divinedrop.DivineDrop;
import org.bukkit.ChatColor;

import java.io.File;
import java.io.IOException;

public class LangManager {

    private DivineDrop plugin = DivineDrop.getInstance();
    private Downloader downloader = new Downloader(plugin);

    public LangManager() { }

    public void downloadLang(String lang) {
        String langPath = Data.getLangPath();
        try {
            File langFolder = new File(plugin.getDataFolder().getAbsolutePath() + "/languages/");
            if(!langFolder.exists()) {
                if(!langFolder.mkdir()) {
                    plugin.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "[DivineDrop] §cI cant create languages folder."));
                    plugin.getServer().getPluginManager().disablePlugin(plugin);
                    return;
                }
            }
            File langFile = new File(langPath);
            if(!langFile.exists()) downloader.downloadResource(lang, new File(langPath));
            new LocaleLanguage();
        } catch (IOException ex) {
            plugin.getLogger().severe(ex.getMessage());
        }

    }
}
